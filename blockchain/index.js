const Block = require('./Block');
const {cryptoHash} = require('../util');

class Index {
    constructor() {
        this.chain = [Block.genesis()];
    }

    addBlock({data}) {
        const newBlock = Block.mineBlock({
            lastBlock: this.chain[this.chain.length - 1],
            data
        });

        this.chain.push(newBlock);
    }

    replaceChain(chain) {
        if (chain.length <= this.chain.length)
        {
            console.error('The incoming chain must be longer.');
            return;
        }

        if (!Index.isValidChain(chain))
        {
            console.error('The incoming chain must be valid');
            return;
        }

        console.log('Replacing chain with ', chain);
        this.chain = chain;
    }

    static isValidChain(chain) {
        if (JSON.stringify(chain[0]) !== JSON.stringify(Block.genesis())) {
            return false;
        }

        for (let i = 1; i < chain.length; i++) {
            //Current block
            const {timestamp, lastHash, hash, data, nonce, difficulty} = chain[i];

            //Previous block hash
            const actualLastHash = chain[i - 1].hash;

            const lastDifficulty = chain[i-1].difficulty;

            //Check if current block lastHash property is equal previous block hash
            if (lastHash !== actualLastHash) {
                return false;
            }

            //Hash as is should be
            const validatedHash = cryptoHash(timestamp, lastHash, data, nonce, difficulty);

            //Check if current block hash is actually correct hash (if data has not be manipulated)
            if (hash !== validatedHash) {
                return false;
            }

            if (Math.abs(lastDifficulty - difficulty) > 1) {
                return false;
            }
        }

        return true;
    }
}

module.exports = Index;