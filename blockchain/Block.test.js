const hexToBinary = require('hex-to-binary');

const Block = require('./Block');
const {GENESIS_DATA, MINE_RATE} = require('../config');
const {cryptoHash} = require('../util');

describe('Block', () => {
    const timestamp = 2000;
    const lastHash = 'foo-hash';
    const hash = 'bar-hash';
    const data = ['blockchain', 'data'];

    const nonce = 1;
    const difficulty = 1


    const block = new Block({timestamp, lastHash, hash, data, nonce, difficulty});

    it('should have timestamp, lastHash, data property', function () {
        expect(block.timestamp).toEqual(timestamp);
        expect(block.lastHash).toEqual(lastHash);
        expect(block.hash).toEqual(hash);
        expect(block.data).toEqual(data);
        expect(block.nonce).toEqual(nonce);
        expect(block.difficulty).toEqual(difficulty);
    });

    describe('genesis()', () => {
        const genesisBlock = Block.genesis();

        it('should return a Block instance', function () {
            expect(genesisBlock).toBeInstanceOf(Block)
        });

        it('should return genesis data', function () {
            expect(genesisBlock).toEqual(GENESIS_DATA)
        });
    })

    describe('mineBlock', () => {
        const lastBlock = Block.genesis();
        const data = 'mined data';

        const minedBlock = Block.mineBlock({lastBlock, data});

        it('should return a Block instance', function () {
            expect(lastBlock).toBeInstanceOf(Block);
        });

        it('sets the `lastHash` to be the `hash` of the lastBlock', function () {
            expect(minedBlock.lastHash).toEqual(lastBlock.hash)
        });

        it('sets the `data`', function () {
            expect(minedBlock.data).toEqual(data);
        });

        it('sets a `timestamp`', function () {
            expect(minedBlock.timestamp).toBeDefined()
        });

        it('creates a SHA-256 `hash` based on the proper inputs', function () {
            expect(minedBlock.hash)
                .toEqual(
                    cryptoHash(
                        minedBlock.timestamp,
                        minedBlock.nonce,
                        minedBlock.difficulty,
                        lastBlock.hash,
                        data
                    ))
        });

        it('should sets a `hash` that matches the difficulty criteria', () => {
            expect(hexToBinary(minedBlock.hash).substring(0, minedBlock.difficulty))
                .toEqual('0'.repeat(minedBlock.difficulty));
        });

        it('adjust difficulty',  () => {
            const possibleResults = [lastBlock.difficulty + 1, lastBlock.difficulty - 1];

            expect(possibleResults.includes(minedBlock.difficulty)).toBe(true);
        });
    });

    describe('adjustDifficulty', () => {
        it('raises difficulty for a quickly mined block', () => {
            expect(Block.adjustDifficulty({
                originalBlock: block,
                timestamp: block.timestamp + MINE_RATE - 100
            })).toEqual(block.difficulty+1)
        });

        it('lowers difficulty for a slowly mined block', () => {
            expect(Block.adjustDifficulty({
                originalBlock: block,
                timestamp: block.timestamp + MINE_RATE + 100
            })).toEqual(block.difficulty-1)
        });

        it('has a lower limit of 1', function () {
            block.difficulty = -1;

            expect(Block.adjustDifficulty({originalBlock: block})).toEqual(1)
        });
    });
});
