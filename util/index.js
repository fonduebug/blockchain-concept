const EC = require('elliptic').ec;

const ec = new EC('secp256k1');
const crypto = require('crypto');

const verifySignature = ({publicKey, data, signature}) => {
    const keyFormPublic = ec.keyFromPublic(publicKey, 'hex');

    return keyFormPublic.verify(cryptoHash(data), signature)
};

const cryptoHash = (...inputs) => {
    const hash = crypto.createHash('sha256');

    hash.update(inputs.map(input => JSON.stringify(input)).sort().join(' '));

    return hash.digest('hex');
};

module.exports = {
    ec,
    verifySignature,
    cryptoHash
};